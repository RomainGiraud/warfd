#ifndef WARFD_CHUNK_H
#define WARFD_CHUNK_H

#include <glm/glm.hpp>

#include <GL/gl_core.hpp>

#include <noise/noise.h>
#include <noise/noiseutils.h>

#include <engine/shader_program.h>
#include <engine/buffer.h>

namespace warfd
{

class Game;

class Chunk
{
public:
    Chunk(Game *game);
    ~Chunk();

    void Load(noise::utils::NoiseMap heightMap);
    void Render(double time);

    glm::vec3 GetPosition() const;
    void SetPosition(glm::vec3 p);

    static glm::ivec3 GetSize();
    
protected:
    Game *_game;

    glm::vec3 _position;
    ShaderProgram *_shader;
    int _indiceLength;
    GLuint _textureID;

    int* _data;

    Buffer _indiceBuffer;
    Buffer _vertexBuffer;
    Buffer _textureBuffer;
    Buffer _normalBuffer;

    static const glm::ivec3 _size;

    void RenderPart(double time, int layer);
};

}

#endif /*WARFD_CHUNK_H*/