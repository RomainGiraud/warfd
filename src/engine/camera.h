#ifndef WARFD_CAMERA_H
#define WARFD_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace warfd
{

class Game;

class Camera
{
public:
    Camera(Game *game);
    ~Camera();

    void Update(double time);

    float GetZoom() const;
    void SetZoom(float z);

    glm::vec3 GetPosition() const;
    void SetPosition(glm::vec3 p);
    
    glm::vec3 GetTargetPosition() const;
    void SetTargetPosition(glm::vec3 p);

    void SetSize(glm::ivec2 size);

    glm::mat4 GetProjectionMatrix() const;
    glm::mat4 GetViewMatrix() const;
    
protected:
    Game *_game;

    glm::vec3 _position;
    float _zoom;
    glm::quat _orientation;

    glm::vec3 _targetPosition;
    glm::quat _targetOrientation;

    glm::mat4 _projectionMatrix;
    glm::mat4 _viewMatrix;

    glm::vec3 _target;
    glm::vec3 _direction;

    glm::ivec2 _size;

    void UpdateProjectionMatrix();
    void UpdateViewMatrix();
};

}

#endif /*WARFD_CAMERA_H*/