#include <engine/terrain.h>

#include <engine/game.h>
#include <engine/chunk.h>
#include <global/tools.h>

#include <noise/noise.h>
#include <noise/noiseutils.h>
using namespace noise;

#include <iostream>
#include <vector>
#include <ctime>
using namespace std;

warfd::Terrain::Terrain(Game *game)
    : _game(game)
{
}

warfd::Terrain::~Terrain()
{
    for (list<Chunk*>::const_iterator it = _chunks.begin(); it != _chunks.end(); ++it)
    {
    	delete (*it);
    }
}

void warfd::Terrain::Load()
{
    module::Perlin myModule;
    myModule.SetOctaveCount(6);
    myModule.SetFrequency(0.5);
    myModule.SetPersistence(0.5);
    //myModule.SetSeed(time(0));

    utils::NoiseMap heightMap;
    utils::NoiseMapBuilderPlane heightMapBuilder;
    heightMapBuilder.SetSourceModule(myModule);
    heightMapBuilder.SetDestNoiseMap(heightMap);
    heightMapBuilder.SetDestSize(Chunk::GetSize().x, Chunk::GetSize().y);

    int size = 1;
	for (int x = 0; x < 5; ++x)
	{
		for (int y = 0; y < 5; ++y)
		{
            heightMapBuilder.SetBounds(x * size, (x+1) * size, y * size, (y+1) * size);
            heightMapBuilder.Build();

            Chunk *ch = new Chunk(_game);
            ch->SetPosition(glm::vec3(x,y,0));
            ch->Load(heightMap);

            _chunks.push_back(ch);
		}
	}
}

void warfd::Terrain::Update(double timeSec)
{
}

void warfd::Terrain::Render(double timeSec)
{
    for (list<Chunk*>::const_iterator it = _chunks.begin(); it != _chunks.end(); ++it)
    {
    	//if (_game->GetCamera().GetBoundingFrustum().Contains((*it)->GetBox()) != ContainmentType::Outside)
    	{
	        (*it)->Render(timeSec);
    	}
    }
}

void warfd::Terrain::Generate()
{
}