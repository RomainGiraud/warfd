#ifndef WARFD_BUFFER_H
#define WARFD_BUFFER_H

#include <GL/gl_core.hpp>

namespace warfd
{

class Buffer
{
public:
    enum BufferType
    {
        Vertex,
        Index
    };

    Buffer(BufferType type = BufferType::Vertex);
    void Bind();
    void Attach(const void *data, size_t size);
    
private:
    BufferType _type;
    GLenum _target;
    GLenum _usage;
    GLuint _buffer;
};
}

#endif /*WARFD_BUFFER_H*/