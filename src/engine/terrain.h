#ifndef WARFD_TERRAIN_H
#define WARFD_TERRAIN_H

#include <list>
#include <vector>

namespace warfd
{

class Game;
class Chunk;

class Terrain
{
public:
  Terrain(Game *game);
  ~Terrain();

  void Load();
  void Update(double timeSec);
  void Render(double timeSec);
    
private:
  Game *_game;
  std::list<Chunk*> _chunks;

  void Generate();
};
}

#endif /*WARFD_TERRAIN_H*/