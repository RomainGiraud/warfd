#include <engine/game.h>

#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <cmath>
using namespace std;

#include <json/reader.h>
using namespace json;

#include <global/exception.h>
#include <global/tools.h>
#include <engine/shader.h>
#include <global/settings.h>
#include <engine/content.h>
#include <engine/camera.h>
#include <engine/terrain.h>
using namespace warfd;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <freeimage/FreeImage.h>


static void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
    cerr << "FreeImage - error: " << FreeImage_GetFormatFromFIF(fif) << endl
         << "    " << message << endl;
}

static void APIENTRY myErrorCallback(GLenum _source, GLenum _type, GLuint _id, GLenum _severity, GLsizei _length, const char* _message, void* _userParam)
{
    cout << "[error] " << _message << endl;
}


warfd::Game* warfd::Game::MainGame = 0;

warfd::Game::Game()
{
    if (MainGame != 0)
        throw Exception("Only one game can be created at a time.");
    MainGame = this;

    glfwInit();
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, false);
    _window = glfwCreateWindow(800, 600, "Warfd", NULL, NULL);
    glfwMakeContextCurrent(_window);
    glfwSwapInterval(0); // vsync: 0 => off, 1 => on

    // Init GLLoader
    gl::sys::LoadFunctions();

    // FreeImage loading
	FreeImage_Initialise();
    FreeImage_SetOutputMessage(FreeImageErrorHandler);

    DisplayGLInfo();

    (void)myErrorCallback;
    //gl::DebugMessageCallbackARB(myErrorCallback, NULL);
    //gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS_ARB);

    // Initialize engine's parts
#if defined(WARFD_SYSTEM_WINDOWS)
    _content = new Content("../resources");
#elif defined(WARFD_SYSTEM_MACOS)
    _content = new Content("/Users/romain/Documents/Projets/warfd/resources");
#endif

    _settings = new Settings(this);
    
    _camera = new Camera(this);
    //_camera->SetPosition(glm::vec3(0, 20, 10));
    //_camera->SetTargetPosition(glm::vec3(0, 20, 10));

    _terrain = new Terrain(this);

    // FPS
    for (int i = 0; i < _fpsSize; ++i)
    {
        _fpsList[i] = 0;
    }

    // Set callbacks
    glfwSetWindowSizeCallback(_window, WindowSizeCallback);
    glfwSetKeyCallback(_window, KeyCallback);
    glfwSetScrollCallback(_window, MouseWheelCallback);

    int width, height;
    glfwGetWindowSize(_window, &width, &height);
    Resize(width, height);
}

warfd::Game::~Game()
{
    // TODO
    /*
    delete _terrain;
    delete _camera;
    delete _content;
    delete _settings;
    */

    glfwTerminate();
}

void warfd::Game::WindowSizeCallback(GLFWwindow *window, int w, int h)
{
    cout << "RESIZE: " << w << ", " << h << endl;
    MainGame->Resize(w, h);
}

void warfd::Game::KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    MainGame->Key(key, scancode, action, mods);
}

void warfd::Game::MouseWheelCallback(GLFWwindow *window, double x, double y)
{
    MainGame->MouseWheel(x, y);
}

void warfd::Game::DisplayGLInfo() const
{
    cout << "OpenGL: " << endl
        << "  - version: " << gl::GetString(gl::VERSION) << endl
        << "  - vendor: " << gl::GetString(gl::VENDOR) << endl
        << "  - renderer: " << gl::GetString(gl::RENDERER) << endl
        << "  - shading version: " << gl::GetString(gl::SHADING_LANGUAGE_VERSION) << endl;
}

void warfd::Game::CursorShow()
{
    glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void warfd::Game::CursorHide()
{
    glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

glm::vec2 warfd::Game::GetMousePosition()
{
    double x, y;
    glfwGetCursorPos(_window, &x, &y);
    return glm::vec2(x, y);
}

void warfd::Game::SetMousePosition(glm::vec2 v)
{
    glfwSetCursorPos(_window, v.x, v.y);
}

glm::ivec2 warfd::Game::GetSize()
{
    int w, h;
    glfwGetWindowSize(_window, &w, &h);
    return glm::ivec2(w, h);
}

warfd::Content& warfd::Game::GetContent()
{
    return *_content;
}

warfd::Settings& warfd::Game::GetSettings()
{
    return *_settings;
}

warfd::Camera& warfd::Game::GetCamera()
{
    return *_camera;
}

void warfd::Game::Load()
{
    _terrain->Load();
}

int warfd::Game::Run()
{
    InitGL();

    double previousTime = glfwGetTime();
    double time = 1;
    while (!glfwWindowShouldClose(_window))
    {
        double currentTime = glfwGetTime();
        time = currentTime - previousTime; // seconds
        previousTime = currentTime;

        _camera->Update(time);

        /*
        if (_input->RenderWireframe())
        {
            gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);
        }
        else
        {
            gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
        }
        */

        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

        // HUD
        double fps = ComputeFPS(1.0f / time); (void)fps;
        _terrain->Render(time);

        glfwSwapBuffers(_window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void warfd::Game::InitGL()
{
    gl::ClearColor(0.39f, 0.58f, 0.93f, 1);
    
    gl::Enable(gl::DEPTH_TEST);

    gl::Enable(gl::BLEND);
    gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

/*
    glEnable(glCULL_FACE);
    glCullFace(glBACK);
    glFrontFace(glCCW);

    glEnable(glDEPTH_TEST);
    glDepthFunc(glLESS);

    glEnable(glBLEND);
    glBlendFunc(glSRC_ALPHA, glONE_MINUS_SRC_ALPHA);

    glEnable(glLINE_SMOOTH);
    glHint(glLINE_SMOOTH_HINT, glNICEST);
    glEnable(glPOLYGON_SMOOTH);
    glHint(glPOLYGON_SMOOTH_HINT, glNICEST);
*/
}

void warfd::Game::Resize(int w, int h)
{
    gl::Viewport(0, 0, w, h);
    _camera->SetSize(GetSize());
}

void warfd::Game::MouseWheel(double x, double y)
{
    _camera->SetZoom(y);
}

void warfd::Game::Key(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(_window, true);

    bool moving = false;
    glm::vec3 mov;
    if (action == GLFW_PRESS || action == GLFW_REPEAT)
    {
        switch (key)
        {
            case GLFW_KEY_Z:
                mov.y += 1;
                moving = true;
                break;
            case GLFW_KEY_S:
                mov.y += -1;
                moving = true;
                break;
            case GLFW_KEY_D:
                mov.x += 1;
                moving = true;
                break;
            case GLFW_KEY_Q:
                mov.x += -1;
                moving = true;
                break;
        }
    }
    if (moving)
    {
        _camera->SetTargetPosition(_camera->GetTargetPosition() - mov);
    }
}

double warfd::Game::ComputeFPS(int value)
{
    _fpsSum -= _fpsList[_fpsIndex];
    _fpsSum += value;
    _fpsList[_fpsIndex] = value;

    if (++_fpsIndex == _fpsSize)
        _fpsIndex = 0;

    return ((double)_fpsSum / _fpsSize);
}