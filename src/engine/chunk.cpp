#include <engine/chunk.h>

#include <engine/game.h>
#include <engine/content.h>
#include <engine/camera.h>

#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <iostream>
#include <vector>
using namespace std;

const glm::ivec3 warfd::Chunk::_size = glm::ivec3(32, 32, 3);

warfd::Chunk::Chunk(Game *game)
    : _textureID(-1),
      _indiceBuffer(Buffer::Index),
      _vertexBuffer(Buffer::Vertex),
      _textureBuffer(Buffer::Vertex),
      _normalBuffer(Buffer::Vertex)
{
  _game = game;
  _data = new int[_size.x * _size.y * _size.z];
}

warfd::Chunk::~Chunk()
{
}

glm::ivec3 warfd::Chunk::GetSize()
{
  return _size;
}

glm::vec3 warfd::Chunk::GetPosition() const
{
  return _position;
}

void warfd::Chunk::SetPosition(glm::vec3 p)
{
  _position = p;
}

// x is the height, y is the width and z is the depth
// height + width * WIDTH + depth * WIDTH * DEPTH
inline int val(int x, int y, int z, glm::ivec3 length)
{
    return y * length.x * length.z + x * length.z + z;
}
#define get(x,y,z) _data[val(x,y,z,_size)]
#define cc(x,y,z) (y * _size.x * _size.z + x * _size.z + z)

void warfd::Chunk::Load(noise::utils::NoiseMap heightMap)
{
  _shader = _game->GetContent().LoadShaders("shaders/phong_tex.vert", "shaders/phong_tex.frag");
  _textureID = _game->GetContent().LoadDDSTexture("textures/current.dds");

  for (int z = 0; z < _size.z; ++z)
  {
    for (int y = 0; y < _size.y; ++y)
    {
      for (int x = 0; x < _size.x; ++x)
      {
        _data[cc(x,y,z)] = 88;
      }
    }
  }

  for (int y = 0; y < _size.y; ++y)
  {
    for (int x = 0; x < _size.x; ++x)
    {
      float val = heightMap.GetValue(x, y);
      val = (val + 1) / 2.0;
      val = val * _size.z;

      for (int z = 0; z <= val; ++z)
      {
        _data[cc(x,y,z)] = 2;
      }
    }
  }

  unsigned int size = _size.x * _size.y * _size.z;
  vector<unsigned int> indices;
  indices.reserve(size * 6);
  vector<float> vertices;
  vertices.reserve(size * 12);
  vector<float> texcoords;
  texcoords.reserve(size * 8);
  vector<float> normals;
  normals.reserve(size * 12);

  float off = 0.f;
  for (int z = 0; z < _size.z; ++z)
  {
    for (int y = 0; y < _size.y; ++y)
    {
      for (int x = 0; x < _size.x; ++x)
      {
        vertices.push_back(0 + x - off);
        vertices.push_back(0 + y - off);
        vertices.push_back(_size.z-z);
        vertices.push_back(1 + x + off);
        vertices.push_back(0 + y - off);
        vertices.push_back(_size.z-z);
        vertices.push_back(1 + x + off);
        vertices.push_back(1 + y + off);
        vertices.push_back(_size.z-z);
        vertices.push_back(0 + x - off);
        vertices.push_back(1 + y + off);
        vertices.push_back(_size.z-z);


        int type = _data[cc(x,y,z)];
        //if (type != 89) type += z;
        float texX = type % 16;
        float texY = type / 16;
        const float texSizz = 0.0625f; // 1 / texNum
        const float texOffset = 0.005f;
        //int t0 = texSizz * 0 + texOffset;
        //int t1 = texSizz * (2) - texOffset;
        float u0 = texX * texSizz + texOffset;
        float u1 = (texX + 1) * texSizz - texOffset;
        float v0 = 1 - (texY * texSizz) - texOffset;
        float v1 = 1 - ((texY + 1) * texSizz) + texOffset;

        texcoords.push_back(u0);
        texcoords.push_back(v1);
        texcoords.push_back(u1);
        texcoords.push_back(v1);
        texcoords.push_back(u1);
        texcoords.push_back(v0);
        texcoords.push_back(u0);
        texcoords.push_back(v0);

        normals.push_back(0);
        normals.push_back(0);
        normals.push_back(1);
        normals.push_back(0);
        normals.push_back(0);
        normals.push_back(1);
        normals.push_back(0);
        normals.push_back(0);
        normals.push_back(1);
        normals.push_back(0);
        normals.push_back(0);
        normals.push_back(1);
      }
    }
  }

  for (unsigned int i = 0, offset = 0; i < size * 6; i += 6, ++offset)
  {
    indices.push_back(0 + offset * 4);
    indices.push_back(1 + offset * 4);
    indices.push_back(2 + offset * 4);
    indices.push_back(0 + offset * 4);
    indices.push_back(2 + offset * 4);
    indices.push_back(3 + offset * 4);
  }

  _indiceBuffer.Attach(indices.data(), indices.size() * sizeof(unsigned int));
  _vertexBuffer.Attach(vertices.data(), vertices.size() * sizeof(float));
  _textureBuffer.Attach(texcoords.data(), texcoords.size() * sizeof(float));
  _normalBuffer.Attach(normals.data(), normals.size() * sizeof(float));

  _indiceLength = indices.size();
}

void warfd::Chunk::Render(double time)
{
  RenderPart(time, 0);
  RenderPart(time, 1);
  RenderPart(time, 2);
}

void warfd::Chunk::RenderPart(double time, int layer)
{
    glm::mat4 projectionMatrix = _game->GetCamera().GetProjectionMatrix();
    glm::mat4 viewMatrix = _game->GetCamera().GetViewMatrix();
    glm::mat4 modelMatrix = glm::translate(_position * glm::vec3(_size));

    glm::mat4 mvMatrix = viewMatrix * modelMatrix;
    glm::mat4 mvpMatrix = projectionMatrix * mvMatrix;
    glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(mvMatrix));

    _shader->Bind();
    _shader->SetUniformValue("mvMatrix", mvMatrix);
    _shader->SetUniformValue("mvpMatrix", mvpMatrix);
    _shader->SetUniformValue("normalMatrix", normalMatrix);
    _shader->SetUniformValue("Light.Position", viewMatrix * glm::vec4(0, 3, 0, 1));
    //_shader->SetUniformValue("Light.Position", viewMatrix * glm::vec4(0.1f, 1, 0.1f, 0));
    _shader->SetUniformValue("Light.Intensity", glm::vec3(1, 1, 1));
    //_shader->SetUniformValue("Material.Ka", glm::vec3(0.329412f, 0.223529f, 0.027451f));
    //_shader->SetUniformValue("Material.Kd", glm::vec3(0.780392f, 0.568627f, 0.113725f));
    //_shader->SetUniformValue("Material.Ks", glm::vec3(0.992157f, 0.941176f, 0.807843f));
    //_shader->SetUniformValue("Material.Shininess", 27.89743616f);
    _shader->SetUniformValue("Material.Ka", glm::vec3(0.2f, 0.2f, 0.2f) * (float)(layer + 0));
    _shader->SetUniformValue("Material.Kd", glm::vec3(0.8f, 0.8f, 0.8f));
    _shader->SetUniformValue("Material.Ks", glm::vec3(0.5f, 0.5f, 0.5f));
    _shader->SetUniformValue("Material.Shininess", 50.0f);

    gl::ActiveTexture(gl::TEXTURE0);
    gl::BindTexture(gl::TEXTURE_2D, _textureID);
    _shader->SetUniformValue("DiffuseTexture", 0);
    
    _shader->EnableVertexAttribArray("in_Position");
    _vertexBuffer.Bind();
    _shader->VertexAttribPointer("in_Position", 3, gl::FLOAT, false, 0, 0);

    _shader->EnableVertexAttribArray("in_TexCoord", false);
    _textureBuffer.Bind();
    _shader->VertexAttribPointer("in_TexCoord", 2, gl::FLOAT, false, 0, 0, false);

    _shader->EnableVertexAttribArray("in_Normal", false);
    _normalBuffer.Bind();
    _shader->VertexAttribPointer("in_Normal", 3, gl::FLOAT, false, 0, 0, false);

    _indiceBuffer.Bind();
    
    gl::Enable(gl::SCISSOR_TEST);
    gl::Scissor(0, 0, 600, 400);
    gl::DrawElements(gl::TRIANGLES, _indiceLength/_size.z, gl::UNSIGNED_INT, (GLvoid *)(_size.x * _size.y * 6 * sizeof(float) * layer));
    gl::Disable(gl::SCISSOR_TEST);
}