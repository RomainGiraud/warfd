#include <engine/camera.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
using namespace std;

warfd::Camera::Camera(Game *game)
    : _game(game)
{
  _zoom = 1;
  _size = glm::vec2(2, 2);
  UpdateViewMatrix();
  UpdateProjectionMatrix();
}

warfd::Camera::~Camera()
{
}

void warfd::Camera::Update(double time)
{
  if (_position != _targetPosition)
  {
    _position = glm::mix(_position, _targetPosition, (float)time * 3);
    UpdateViewMatrix();
  }
}

void warfd::Camera::UpdateProjectionMatrix()
{
  float halfx = (_size.x / 2.0f) * _zoom;
  float halfy = (_size.y / 2.0f) * _zoom;
  // left, right, bottom, top, znear, zfar
  _projectionMatrix = glm::ortho(-halfx, halfx, -halfy, halfy, 0.f, -100.f);
}

void warfd::Camera::UpdateViewMatrix()
{
  _viewMatrix = glm::translate(glm::mat4(1.0), _position);
}

void warfd::Camera::SetSize(glm::ivec2 size)
{
  _size = size;
  UpdateProjectionMatrix();
}

glm::vec3 warfd::Camera::GetTargetPosition() const
{
  return _targetPosition;
}

void warfd::Camera::SetTargetPosition(glm::vec3 p)
{
  _targetPosition = p;
}

glm::mat4 warfd::Camera::GetProjectionMatrix() const
{
  return _projectionMatrix;
}

glm::mat4 warfd::Camera::GetViewMatrix() const
{
  return _viewMatrix;
}

glm::vec3 warfd::Camera::GetPosition() const
{
  return _position;
}

void warfd::Camera::SetPosition(glm::vec3 p)
{
  //_position = _targetPosition = p;
  _position = p;
  UpdateViewMatrix();
}

float warfd::Camera::GetZoom() const
{
  return _zoom;
}

void warfd::Camera::SetZoom(float z)
{
  _zoom += z / 50;
  if (_zoom < 0.05)
    _zoom = 0.05f;

  UpdateProjectionMatrix();
}