#ifndef WARFD_SHADER_H
#define WARFD_SHADER_H

#include <GL/gl_core.hpp>

#include <string>

namespace warfd
{

class Shader
{
public:
    enum ShaderType
    {
        Vertex,
        //Geometry,
        Fragment
    };

    Shader(ShaderType type);
    void LoadFile(const std::string &filename);
    void LoadSource(const std::string &source);
    
    GLuint Id() const;

private:
    GLuint _id;
    ShaderType _type;

    void Compile(const char *src);
};

}

#endif /*WARFD_SHADER_H*/