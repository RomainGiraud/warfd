#ifndef WARFD_GAME_H
#define WARFD_GAME_H

#include <list>
#include <string>

#include <glm/glm.hpp>

#include <GL/gl_core.hpp>

#define GLFW_NO_GLU
#include <GL/glfw3.h>

#include <global/global.h>

namespace warfd
{

class Settings;
class Content;
class Camera;
class Terrain;

class Game
{
public:
    Game();
    ~Game();
    void DisplayGLInfo() const;

    void LoadScene(std::string filename);

    void Load();
    int Run();
    
    void CursorShow();
    void CursorHide();
    glm::vec2 GetMousePosition();
    void SetMousePosition(glm::vec2 v);
    glm::ivec2 GetSize();

    Content& GetContent();
    Settings& GetSettings();
    Camera& GetCamera();

private:
    static Game *MainGame;
    GLFWwindow *_window;

    Settings *_settings;
    Content *_content;
    Camera *_camera;
    Terrain *_terrain;

    void InitGL();

    // FPS tool
    static const int _fpsSize = 100;
    int _fpsList[_fpsSize];
    int _fpsIndex = 0;
    int _fpsSum = 0;
    double ComputeFPS(int value);

    // Callbacks
    static void WindowSizeCallback(GLFWwindow *window, int w, int h);
    static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
    static void MouseWheelCallback(GLFWwindow *window, double x, double y);
    void Resize(int w, int h);
    void MouseWheel(double x, double y);
    void Key(int key, int scancode, int action, int mods);
};

}

#endif /*WARFD_GAME_H*/
