#ifndef WARFD_SETTINGS_H
#define WARFD_SETTINGS_H

#include <glm/glm.hpp>

#include <string>

namespace warfd
{

class Game;

enum InputKey
{
  Forward,
  Backward,
  Rightward,
  Leftward
};

class Settings
{
public:
  Settings(Game *game);
  void Load(std::string file);

  char GetKey(InputKey key) const;
    
private:
	Game *_game;
};
}

#endif /*WARFD_SETTINGS_H*/