#ifndef WARFD_GLOBAL_H
#define WARFD_GLOBAL_H

// references: http://sourceforge.net/p/predef/wiki/OperatingSystems/

// Bring in OpenGL
// Windows
#ifdef _WIN32
#define WARFD_SYSTEM_WINDOWS
#endif

// Mac OS X
#ifdef __APPLE__
#define WARFD_SYSTEM_MACOS
#endif

// Linux
#ifdef __linux__
#define WARFD_SYSTEM_LINUX
#endif

#endif /*WARFD_GLOBAL_H*/
