#include <iostream>
using namespace std;

#include <engine/game.h>
#include <global/version.h>
#include <global/global.h>
#include <global/exception.h>
using namespace warfd;

int main()
{
    cout << "version: " << WARFD_VERSION_STRING << endl;

    try
    {
        Game game;
        game.Load();
        return game.Run();
    }
    catch (const Exception &e)
    {
        cerr << "====> " << e << endl;

#ifdef WARFD_SYSTEM_WINDOWS
        system("pause");
#endif /*WARFD_SYSTEM_WINDOWS*/
    }

    return 0;
}
